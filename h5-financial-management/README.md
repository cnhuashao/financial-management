[混合模式](https://docs.antdv-pro.com/guide/server.html#%E6%B7%B7%E5%90%88%E4%BD%BF%E7%94%A8)
[移除mist](https://docs.antdv-pro.com/mist/remove.html)
[移动端模版](https://vant-ui.github.io/)

[Vue3、Vite4、ant-design-vue4、Pinia、UnoCSS和Typescript]()

打包
npx vite build

查看项目详情依赖
vue ui

## 特性
- pnpm：使用了最新的pnpm作为包管理工具，它可以大大减少node_modules的体积，加快包的安装速度，同时还可以共享依赖，减少磁盘占用。
- vite：vite作为前端开发工具，它可以大大加快项目的启动速度，同时还支持热更新，可以大大提高开发效率。
- vue3：vue3作为前端框架，基础代码全部使用script-setup的写法，代码量少维护成本低。
- ant-design-vue4：ant-design-vue4作为UI框架，admin-pro的作者也是ant-design-vue的核心成员，可提供长期的维护支持。
- pinia：pinia作为状态管理工具，它可以大大提高代码的可读性和可维护性，同时还支持Typescript。
- UnoCSS：原子化的CSS框架，减少我们去想一些通用类名带来的烦恼，提升我们的开发效率。
- 代码规范：我们封装了一套基于eslint的代码规范配置文件，开箱即用，统一不同团队所带来的问题。
- 主题：延用了react版本的antd-pro的设计规范，开发了一套基于vue的主题模式，在此基础上增加了一些新的功能，尽可能的满足各种不同的需求。
- 请求函数：基于axios封装了一套具有完善类型的请求函数，以及一些基础的拦截器的封装，只需要按照需求做对应的实现调整就能满足各种项目带来的不一样的需求。
- 移动端兼容：基础框架部分我们尽可能的对移动端的模式进行了兼容处理，但是由于我们的主要目标是企业级中后台产品，所以我们并没有对移动端做过多的适配，如果你的项目需要移动端的适配，可以参考我们的代码进行相应的调整。

antdv-pro
├─ public ## 静态资源文件夹
├─ scripts ## 工程脚本文件
├─ themes ## 主题文件夹
├─ servers ## nitro mock服务文件夹
├─ types ## 类型声明文件夹c
├─ src ## 主项目文件夹
│  ├─ App.vue ## 组件入口
│  ├─ assets  ## 静态资源文件夹
│  ├─ components ## 组件文件夹会这里的组件会自动导入
│  ├─ composables ## 组合式api文件夹，默认会自动导入
│  ├─ config ## 配置文件夹
│  │  └─ default-setting.ts ## 主题配置文件
│  ├─ layouts ## 布局文件夹
│  ├─ main.ts ## 项目整体入口
│  ├─ pages ## 页面文件夹
│  ├─ router ## 路由配置文件
│  │  ├─ dynamic-routes.ts ## 动态路由文件夹，这里面配置的会同步生成菜单
│  │  ├─ generate-route.ts ## 生成动态路由结构
│  │  ├─ router-guard.ts ## 路由拦截
│  │  └─ static-routes.ts ## 静态路由
│  ├─ stores ## pinia配置文件夹，默认支持自动导入
│  └─ utils ## 工具函数
├─ .env ## 默认环境配置文件
├─ .env.development ## 开发环境配置文件
├─ .eslintignore ## eslint忽略文件
├─ .eslintrc ## eslint配置文件
├─ index.html
├─ tsconfig.json ## ts配置文件
├─ tsconfig.node.json ## vite.config.ts的ts配置
├─ package.json ## 依赖描述文件
├─ pnpm-lock.yaml ## pnpm包管理版本锁定文件
├─ unocss.config.ts ## unocss配置文件
├─ vercel.json ## 发布到vercel配置文件
└─ vite.config.ts ## vite配置文件