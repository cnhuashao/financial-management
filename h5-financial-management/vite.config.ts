import { resolve } from 'node:path';
import { fileURLToPath } from 'node:url';
import { defineConfig,loadEnv } from 'vite';
import unocss from 'unocss';
import type { ConfigEnv, UserConfig } from 'vite';
import { OUTPUT_DIR } from './plugins/constants';
import { createVitePlugins } from './plugins';

const baseSrc = fileURLToPath(new URL('./src', import.meta.url));

// https://vitejs.dev/config/
export default defineConfig(({ mode }: ConfigEnv): UserConfig => {
    const env = loadEnv(mode, process.cwd(), 'VITE_');

    const proxyObj = {};
    if (mode === 'development' && env.VITE_APP_BASE_API_DEV && env.VITE_APP_BASE_URL_DEV) {
        proxyObj[env.VITE_APP_BASE_API_DEV] = {
            target: env.VITE_APP_BASE_URL_DEV,
            changeOrigin: true,
            rewrite: path => path.replace(new RegExp(`^${env.VITE_APP_BASE_API_DEV}`), ''),
        };
    }

    return {
        base: '/app/',

        // 动态导入polyfill
        optimizeDeps: {
            include: ['@vue/reactivity'], // 强制包含动态导入的库
        },

        // 服务器配置
        server: {
            port: 6679,
            https: false,
            host: '0.0.0.0',
            hmr: true,
            proxy: {
                ...proxyObj,
            },
        },

        // 构建配置
        build: {
            minify: true,
            outDir: OUTPUT_DIR,
            assetsDir: 'assets',
            chunkSizeWarningLimit: 4096,
            rollupOptions: {
                output: {
                    manualChunks: {
                        vue: ['vue', 'vue-router', 'pinia', 'vue-i18n', '@vueuse/core'],
                        antd: ['ant-design-vue', '@ant-design/icons-vue', 'dayjs'],
                    },
                },
            },
        },

        // 插件配置
        plugins: createVitePlugins(env),

        // 解析选项
        resolve: {
            extensions: ['.js', '.jsx', '.ts', '.tsx'],
            alias: [
                { find: '~@', replacement: baseSrc },
                { find: '~', replacement: baseSrc },
                { find: '@', replacement: baseSrc },
                { find: '~#', replacement: resolve(baseSrc, './enums') }
            ]
        },
    };
})