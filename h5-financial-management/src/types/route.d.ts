import 'vue-router'

declare module 'vue-router'{
  import type { RouteRecordRaw } from 'vue-router'

  interface RouteMeta {
    title?: string // 菜单标题
    icon?: string // 菜单图标
    hideInMenu?: boolean // 在菜单中隐藏
    parentKeys?: string[] // 隐藏菜单配置选中的父级菜单
    isIframe?: boolean
    url?: string // iframe模式，会自动使用这个url
    hideInBreadcrumb?: boolean// 在面包屑中隐藏
    hideChildrenInMenu?: boolean // 在菜单中隐藏子节点
    keepAlive?: boolean // 是否进行保活配置
    target?: '_blank' | '_self' | '_parent' // 链接打开方式，当配置的path为全连接的时候
    affix?: boolean // 开启多页签模式的情况下，是否作为固定的页签展示
    id?: string | number
    parentId?: string | number | null
    access?: (string | number)[] // 权限管理，如果当前用户没有这个权限，那么将不会显示这个菜单
    locale?: string
    parentName?: string
    parentComps?: RouteRecordRaw['component'][]
    originPath?: string
  }
}
