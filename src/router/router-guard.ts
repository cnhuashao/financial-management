import { AxiosError } from 'axios'
import router from '~/router'
import { useMetaTitle } from '~/composables/meta-title'
import { setRouteEmitter } from '~@/utils/route-listener'

const allowList = ['/login', '/error', '/401', '/404', '/403']
const loginPath = '/login'

router.beforeEach(async (to, _, next) => {
  setRouteEmitter(to)
  // 获取
  const userStore = useUserStore()
  const token = useAuthorization()
  console.log(`当前会话：${token.value}`)
  if (!token.value) {
    //  如果token不存在就跳转到登录页面
    if (!allowList.includes(to.path) && !to.path.startsWith('/redirect')) {
      console.log("跳转到登录页面:"+to.fullPath);
      next({
        path: loginPath,
        /*query: {
          redirect: encodeURIComponent(to.fullPath),
        },*/
      })
      return
    }
  }
  else {
    if (!userStore.userInfo && !allowList.includes(to.path) && !to.path.startsWith('/redirect')) {
      try {
        console.log('开始获取用户信息')
        // 获取用户信息
        await userStore.getUserInfo()
        // 获取路由菜单的信息
        const currentRoute = await userStore.generateDynamicRoutes()
        router.addRoute(currentRoute)
        console.log('Redirecting with replace:', to);
        // 如果是跳转到 http开头的，就跳转到首页
        if (to.fullPath.startsWith('/http')) {
          to.fullPath = "/";
          to.path = "/";
        }
        console.log("1111-" + to.path+"-"+to.fullPath);
        next({
          ...to,
          replace:true,
        })
        return
      }
      catch (e) {
        if (e instanceof AxiosError && e?.response?.status === 401) {
          // 跳转到error页面
          next({
            path: '/401',
          })
        }
      }
    }
    else {
      // 如果当前是登录页面就跳转到首页
      if (to.path === loginPath) {
        console.log("再次跳转，跳转到跟目录")
        next({
          path: '/',
          replace: true,
        })
        return
      }
    }
  }
  next()
})

router.afterEach((to) => {
  useMetaTitle(to)
  useLoadingCheck()
  useScrollToTop()
})
