// 采购单列表响应结构体
interface PurchaseMess {
  list: PurchaseDetailModel[]
  count: number

}
// 采购信息单
interface PurchaseDetailModel {
  purchaseId: number // 采购单号
  purchaseTotalPrice: string // 采购自动总价, 注意：在前端通常将Decimal转换为字符串处理
  purchaseActualAmount: string // 实际采购总金额
  purchaseVehicle: string // 采购车辆
  purchaseDescribe: string // 采购描述
  purchaseUser: string // 采购人
  purchaseStatus: number // 采购状态，1暂存，2提交保存，3作废
  purchasePaymentStatus: number // 采购单付款状态，1未付款，2部分付款，3全部付款
  purchaseFreight: number // 运费
  purchaseUnitCost: string // 采购单价（成本单价）
  purchaseTotal: number // 总数量，采购总斤数
  remainSettlementAmount: number // 剩余结算总金额
  purchaseCreateTime: string // 采购单创建时间
  purchaseDeparture: string // 采购发车日期
  items: PurchaseDetailItemModel[] // 采购明细项
  // 当前页数
  pageNumber?: number
  // 每页条数
  pageSize?: number
}

// PurchaseDetailItemVueModel
// 采购明细单
interface PurchaseDetailItemModel {
  // 采购明细编号
  itemId: number
  // 关联采购明细ID
  purchaseId: number
  // 产品ID
  productId: number
  // 产品名称
  productName: string
  // 数量，斤数
  quantity: number
  // 采购单价
  price: number
  // 采购自动总价
  totalPrice: number
  // 关联客户ID
  customerId: number
  // 客户名称
  customerName: string
  // 采购实际总价
  actualAmount: number
  // 采购日期, 格式应与后端日期格式一致，如'YYYY-MM-DD'
  itemDate: string
  // 采购类型，1正常采购，2全额抵消，3部分抵消
  itemType: number
  // 结算状态，1已结算，2未结算，3部分结算
  settlementStatus: number
  // 已结算金额
  settlementAmount: number
  // 剩余结算金额，自动计算
  irSettlementAmount: number
}

type PurchaseDetailModelParams = Partial<Omit<PurchaseDetailModel, 'purchaseId'>>

export async function getPurchaseListAll() {
  return usePost('/purchase/getPurchaseListAll', undefined, {
    customDev: true,
    loading: true,
  })
}

// 获取采购信息列表
export async function getPurchaseList(params?: PurchaseDetailModelParams) {
  return usePost<PurchaseMess>('/purchase/getPurchaseList', params, {
    customDev: true,
    loading: true,
  })
}

// 添加采购信息
export async function createPurchaseInfo(purchaseDetail: PurchaseDetailModel) {
  return usePost('/purchase/createPurchaseInfo', purchaseDetail, {
    customDev: true,
    loading: true,
  })
}

// 获取采购信息详情
export async function getPurchaseInfo(params?: PurchaseDetailModel) {
  return usePost<PurchaseDetailModel>(`/purchase/getPurchaseInfo`, params, {
    customDev: true,
    loading: true,
  })
}

// 删除指定采购单信息
export async function delPurchaseInfo(params?: PurchaseDetailModel) {
  return usePost(`/purchase/delPurchaseInfo`, params, {
    customDev: true,
    loading: true,
  })
}

// 修改采购信息
export async function updatePurchaseInfo(params: PurchaseDetailModel) {
  return usePost('/purchase/updatePurchaseInfo', params, {
    customDev: true,
    loading: true,
  })
}

// 添加采购明细
export async function addPurchaseDetailItem(params: PurchaseDetailItemModel) {
  return usePost('/purchase/addPurchaseDetailItem', params, {
    customDev: true,
    loading: true,
  })
}

// 删除指定采购单明细信息
export async function deletePurchaseDetailItem(params: PurchaseDetailModel) {
  return usePost(`/purchase/delPurchaseDetailItem`, params, {
    customDev: true,
    loading: true,
  })
}

// 修改采购明细
export async function updatePurchaseDetailItem(detailItem: PurchaseDetailItemModel) {
  return usePost('/purchase/updatePurchaseDetailItem', detailItem, {
    customDev: true,
    loading: true,
  })
}

export type {
  PurchaseDetailModel,
  PurchaseDetailItemModel,
  PurchaseDetailModelParams,
  PurchaseMess,
}
