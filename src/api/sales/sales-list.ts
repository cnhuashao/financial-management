// @Description: 销售单接口

import {ResponseBody} from "~/utils/request.ts";

interface SalesDetailModel {
  // 销售订单编号
  salesId: number
  // 销售类型，1正常销售，2全额顶销，3部分顶销
  salesType: number
  // 自动总金额
  salesTotalPrice: number
  // 实际销售总金额
  salesActualTotalPrice: number
  // 总计销售纯利润
  salesPureProfit: number
  // 备注
  salesRemarks: string
  // 销售订单状态，1暂存，2提交保存，3作废
  salesStatus: number
  // 销售人
  salesPerson: string
  // 创建时间
  salesCreateTime: number
  // 关联采购订单ID，指向purchase_detail表的purchase_id
  purchaseId: number
  // 关联采购订单名称
  purchaseName: string
  // 销售明细项列表
  items: SalesDetailItem[] // 销售明细项列表
}

// SalesDetailItem
// @Description: 销售明细接口
interface SalesDetailItem {
  // 销售明细编号
  sdiId: number
  // 关联销售单ID，指向sales_detail表的sales_id
  salesId: number
  // 产品ID
  productId: number
  // 产品名称
  productName: string
  // 销售数量
  sdiQuantity: number
  // 销售单价
  sdiPrice: number
  // 自动总金额
  totalPrice: number
  // 实际总金额
  sdiActualAmount: number
  // 成本单价，采购单中的采购单价（成本单价）
  sdiCostUnitPrice: number
  // 成本总价，采购单价*销售数量
  sdiFullCostPrice: number
  // 纯利润
  sdiPureProfit: number
  // 关联客户ID，指向customer表的customer_id
  customerId: number
  // 客户名称
  customerName: string
  // 销售日期
  sdiDate: string
}

// 定义SalesDetailMess模型以匹配销售信息列表的响应结构
interface SalesDetailMess {
  list: SalesDetailModel[]
  count: number
}

// 添加销售明细
export async function addSalesDetailItem(params?: SalesDetailItem) {
  return usePost(`/sales/addSalesDetailItem`, params, {
    customDev: true,
    loading: true,
  })
}

// 依据采购单信息进行更新销售单中的各项内容
export async function updateSalesInfoOnPurchase(params?: SalesDetailItem) {
  return usePost(`/sales/updateSalesInfoOnPurchase`, params, {
    customDev: true,
    loading: true,
  })
}

// 删除指定销售单信息
export async function deleteSalesDetailInfo(params?: SalesDetailModel) {
  return usePost(`/sales/deleteSalesDetailInfo`, params, {
    customDev: true,
    loading: true,
  })
}
// 删除指定销售明细信息
export async function deleteSalesDetailItemInfo(params?: SalesDetailItem) {
  return usePost(`/sales/deleteSalesDetailItemInfo`, params, {
    customDev: true,
    loading: true,
  })
}

// 修改指定销售明细信息
export async function updateSalesDetailItem(params?: SalesDetailItem) {
  return usePost(`/sales/updateSalesDetailItem`, params, {
    customDev: true,
    loading: true,
  })
}

// 获取销售信息列表的异步函数
export async function getSalesDetailList(params?: SalesDetailSearchParams): Promise<ResponseBody<SalesDetailMess>> {
  return usePost<SalesDetailMess>('/sales/getSalesDetailList', params, {
    customDev: true,
    loading: true,
  })
}

// 获取指定销售信息详情
export async function getSalesDetailInfo(params?: SalesDetailModel) {
  return usePost<SalesDetailModel>('/sales/getSalesDetailInfo', params, {
    customDev: true,
    loading: true,
  })
}

// 定义SalesDetailSearchParams以匹配可能的查询参数
interface SalesDetailSearchParams extends Partial<Omit<SalesDetailModel, 'salesId' | 'items'>> {
  pageNumber?: number
  pageSize?: number
}

// 添加销售信息的异步函数
export async function addSalesDetail(salesDetail: Omit<SalesDetailModel, 'salesId' | 'items'>) {
  return usePost('/sales/addSalesDetail', salesDetail, {
    customDev: true,
    loading: true,
  })
}

// 修改销售信息的异步函数
export async function updateSalesDetail(salesDetail: SalesDetailSearchParams) {
  return usePost(`/sales/updateSalesDetail`, salesDetail, {
    customDev: true,
    loading: true,
  })
}

export type{
  SalesDetailModel,
  SalesDetailMess,
  SalesDetailItem,
}
