import type { MenuData } from '~@/layouts/basic-layout/typing'

// 获取路由菜单
export function getRouteMenusApi() {
  return usePost<MenuData>('/getMenuList',undefined,{
    customDev: true,
  })
}
