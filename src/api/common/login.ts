
export interface LoginParams {
  account: string
  cipher: string
}

export function loginApi(params: LoginParams) {
  console.log("进入方法")
  return usePost(`/login`,params,{
    // 开发模式下使用自定义的接口
    customDev: true,
    // 是否开启全局请求loading
    loading: true,
  });
}

export function logoutApi() {
  return useGet('/logout')
}
