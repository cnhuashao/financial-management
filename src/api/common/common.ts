interface ProvincesModel{
    // 省份编号
    code: number
    // 省份名称
    name: string
    list: CitiesModel[]
}
interface CitiesModel{
    // 城市编号
    code: number
    // 城市名称
    name: string
    // 所属省份编号
    provinceCode: number
}

interface AnnualProcurementModel {
    purchaseVehicle: string
    sjzj: number
    zjs: number
    zyf: number
}

export async function getProvincesCities() {
    return usePost<ProvincesModel[]>('/getProvincesCities',null,{
        // 开发模式下使用自定义的接口
        customDev: true,
        // 是否开启全局请求loading
        loading: true,
    })
}

// 获取特定年份下不同采购车辆的实际采购总金额、总数量、总运费
export async function getAnnualProcurementOfVehicles(yearId: { yearId: string }) {
    return usePost<AnnualProcurementModel[]>('/common/getAnnualProcurementOfVehicles', yearId, {
        // 开发模式下使用自定义的接口
        customDev: true,
        // 是否开启全局请求loading
        loading: true,
    })
}

export type {
    ProvincesModel,
    AnnualProcurementModel,
}