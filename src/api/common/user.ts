export interface UserInfo {
  userID: number | string
  account: string
  username: string
  // nickname: string
  // avatar: string
  roleID: number
}

export function getUserInfoApi() {
  return usePost<UserInfo>('/getUserInfo',undefined,{
      // 开发模式下使用自定义的接口
      customDev: true,
      // 是否开启全局请求loading
      loading: true,
  })
}
