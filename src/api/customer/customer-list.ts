

interface CustomerArr {
    list: CustomerModel[]
    count: number
}

interface CustomerModel {
    // 客户编号
    customerId: number
    // 客户类型
    customerType: number
    // 客户名称
    name: string
    // 建档时间
    recordTime: number
    // 客户性别，1男2女
    gender: number
    // 描述
    description: string
    // 客户所属省份ID
    provinceId: number
    // 客户所属城市ID
    cityId: number
    // 客户具体地址
    address: string
    // 当前页数
    pageNumber?: number
    // 每页条数
    pageSize?: number
}

type CustomerModelParams = Partial<Omit<CustomerModel, 'customerId'>>

// 获取所有客户信息，不带分页，区分客户类型
export async function getCustomerListAll(params?: CustomerModel) {
  return usePost<CustomerModel[]>('/customer/getCustomerListAll', params, {
    // 开发模式下使用自定义的接口
    customDev: true,
    // 是否开启全局请求loading
    loading: true,
  })
}

// 获取所有客户信息，带分页
export async function getCustomerList(params?: CustomerModelParams) {
    return usePost<CustomerArr>('/customer/getCustomerList',params,{
        // 开发模式下使用自定义的接口
        customDev: true,
        // 是否开启全局请求loading
        loading: true,
    })
}

// 创建客户信息
export async function createCustomerInfo(params?: CustomerModelParams) {
    return usePost('/customer/createCustomerInfo',params,{
        // 开发模式下使用自定义的接口
        customDev: true,
        // 是否开启全局请求loading
        loading: true,
    })
}
// 修改客户信息
export async function updateCustomerInfo(params?: CustomerModelParams) {
    return usePost('/customer/updateCustomerInfo',params,{
        // 开发模式下使用自定义的接口
        customDev: true,
        // 是否开启全局请求loading
        loading: true,
    })
}
export async function delCustomerInfo(params?: CustomerModelParams) {
    return usePost('/customer/delCustomerInfo',params,{
        // 开发模式下使用自定义的接口
        customDev: true,
        // 是否开启全局请求loading
        loading: true,
    })
}

export type {
    CustomerArr,
    CustomerModel,
}