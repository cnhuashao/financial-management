interface InventoryMess {
  list: InventoryModel[]
  count: number
}

interface InventoryModel {
  inventoryId: number
  quantity: number
  inventoryType: InventoryTypeModel
}

interface InventoryTypeModel {
  typeId: number
  // 类型名称
  typeName: string
  // 类型描述
  typeDescription?: string
  // 父级类型ID
  parentTypeId?: number
  // 父级类型名称
  parentTypeName?: string
  // 类型使用单位
  unit: string
  // 创建时间
  createdAt: number
  // 当前页数
  pageNumber?: number
  // 每页条数
  pageSize?: number
}

type InventoryModelParams = Partial<Omit<InventoryTypeModel, 'typeId'>>

export async function getInventoryList(params?: InventoryModelParams) {
  return usePost<InventoryMess>('/inventory/getInventoryList', params, {
    // 开发模式下使用自定义的接口
    customDev: true,
    // 是否开启全局请求loading
    loading: true,
  })
}

// 查询所有仓库类型接口
export async function getInventoryTypeList(params?: InventoryModelParams) {
  return usePost<InventoryTypeModel[]>('/inventory/getInventoryTypeList', params, {
    // 开发模式下使用自定义的接口
    customDev: true,
    // 是否开启全局请求loading
    loading: true,
  })
}

// 删除库存类型接口
export async function delInventoryTypeApi(params: InventoryTypeModel) {
  return usePost<InventoryTypeModel[]>('/inventory/delInventoryType', params, {
    // 开发模式下使用自定义的接口
    customDev: true,
    // 是否开启全局请求loading
    loading: true,
  })
}

// 新增库存类型接口
export async function createInventoryType(params: InventoryTypeModel) {
  return usePost('/inventory/createInventoryType', params, {
    // 开发模式下使用自定义的接口
    customDev: true,
    // 是否开启全局请求loading
    loading: true,
  })
}

export type{
  InventoryTypeModel,
  InventoryModel,
  InventoryMess,
}
