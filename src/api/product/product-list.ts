// 产品消息
interface ProductMess {
  list: ProductModel[]
  count: number
}

// 产品类型
interface ProductModel {
  // 产品编号
  productId: number
  // 产品名称
  productName: string
  // 产品描述
  productDesc?: string
  // 成本单价
  cbdj: number
  // 当前页数
  pageNumber?: number
  // 每页条数
  pageSize?: number
}

type ProductModelParams = Partial<Omit<ProductModel, 'productId'>>

// 获取产品信息列表，分页
export async function getProductList(params?: ProductModelParams) {
  return usePost<ProductMess>('/product/getProductList', params, {
    // 开发模式下使用自定义的接口
    customDev: true,
    // 是否开启全局请求loading
    loading: true,
  })
}

// 依据采购id获取产品信息列表
export async function getProductListForPurchaseId(params: any) {
  return usePost<ProductModel[]>('/product/getProductListForPurchaseId', params, {
    // 开发模式下使用自定义的接口
    customDev: true,
    // 是否开启全局请求loading
    loading: true,
  })
}

// 获取所有产品信息，不分页
export async function getProductListAll() {
  return usePost<ProductModel[]>('/product/getProductListAll', null, {
    // 开发模式下使用自定义的接口
    customDev: true,
    // 是否开启全局请求loading
    loading: true,
  })
}

// 删除产品信息
export async function delProductInfo(params?: ProductModel) {
  return usePost('/product/delProductInfo', params, {
    // 开发模式下使用自定义的接口
    customDev: true,
    // 是否开启全局请求loading
    loading: true,
  })
}

// 修改产品信息
export async function updateProductInfo(params?: ProductModel) {
  return usePost('/product/updateProductInfo', params, {
    // 开发模式下使用自定义的接口
    customDev: true,
    // 是否开启全局请求loading
    loading: true,
  })
}

// 创建产品信息
export async function createProductInfo(params?: ProductModel) {
  return usePost('/product/createProductInfo', params, {
    // 开发模式下使用自定义的接口
    customDev: true,
    // 是否开启全局请求loading
    loading: true,
  })
}
// 获取指定产品信息
export async function getProductInfo(params?: ProductModel) {
  return usePost('/product/getProductInfo', params, {
    // 开发模式下使用自定义的接口
    customDev: true,
    // 是否开启全局请求loading
    loading: true,
  })
}

export type {
  ProductModel,
  ProductMess,
}
